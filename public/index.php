<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

$config = include(__DIR__ . '/../app/config.php');

define('APP_PATH', realpath(__DIR__ . '/../app'));

spl_autoload_register(
    function ($class) {
        $path = APP_PATH . '/' . str_replace('\\', '/', $class) . '.php';

        if (file_exists($path)) {
            include ($path);
            return true;
        }

        return false;
    }
);

include_once (__DIR__ . '/../vendor/twig/twig/lib/Twig/Autoloader.php');
Twig_Autoloader::register();
$application = new Library\Application($config);
echo $application->run();