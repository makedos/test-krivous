<?php

namespace Model;


use Library\ArrayFilter\AbstractArrayFilter;
use Library\ArraySort\AbstractArraySort;
use Model\Source\AbstractSource;

/**
 * Dictionary class for getting static data
 *
 * Class Dictionary
 * @package Model
 */
class Dictionary
{
    public function getFilterFields()
    {
        return [
            AbstractSource::FIELD_NAME,
            AbstractSource::FIELD_CODE,
            AbstractSource::FIELD_PRICE
        ];
    }

    public function getSources()
    {
        return [
            AbstractSource::EXTENSIION_PHP,
            AbstractSource::EXTENSION_JSON,
            AbstractSource::EXTENSION_XML
        ];
    }

    public function getGroups()
    {
        return [
            'world',
            'europe'
        ];
    }

    public function getSortMethods()
    {
        return [
            AbstractArraySort::SORT_METHOD_DESC,
            AbstractArraySort::SORT_METHOD_ASC
        ];
    }

    public function getFilterTypes()
    {
        return [
            AbstractArrayFilter::FILTER_TYPE_MORE,
            AbstractArrayFilter::FILTER_TYPE_LESS,
            AbstractArrayFilter::FILTER_TYPE_LIKE
        ];
    }
} 