<?php

namespace Model\Source;

/**
 * Abstract class of source of data
 *
 * Class AbstractSource
 * @package Model\Source
 */
abstract class AbstractSource
{
    const EXTENSIION_PHP  = 'php';
    const EXTENSION_JSON  = 'json';
    const EXTENSION_XML   = 'xml';

    const FIELD_NAME  = 'name';
    const FIELD_PRICE = 'price';
    const FIELD_CODE  = 'code';
    const FIELD_GROUP = 'group';

    /**
     * @param $sourceFileExtension
     * @return Json|Php|Xml
     * @throws \InvalidArgumentException
     */
    public static function factory($sourceFileExtension)
    {
        switch ($sourceFileExtension) {
            case self::EXTENSIION_PHP:
                return new Php();
                break;
            case self::EXTENSION_JSON:
                return new Json();
                break;
            case self::EXTENSION_XML:
                return new Xml();
                break;
            default:
                throw new \InvalidArgumentException('Wrong source file extension!');
        }
    }

    /**
     * Returns path to file with data
     * @return string
     */
    protected function getFilePath()
    {
        return __DIR__ . '/data/data.' . $this->getFileExtension();
    }

    /**
     * Prepare data from file and returns it
     * @return array
     */
    abstract public function getData();

    /**
     * @return string
     */
    abstract protected function getFileExtension();
} 