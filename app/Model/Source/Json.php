<?php
namespace Model\Source;


class Json extends AbstractSource
{
    public function getData()
    {
        $data = json_decode(file_get_contents($this->getFilePath()), true);

        $allCurrencies = [];
        foreach ($data as $currency) {
            $allCurrencies[] = [
                self::FIELD_CODE  => $currency[0],
                self::FIELD_NAME  => $currency[1],
                self::FIELD_PRICE => str_replace(',', '.', $currency[2]),
                self::FIELD_GROUP => $currency[3],
            ];
        }

        return $allCurrencies;
    }

    protected function getFileExtension()
    {
        return self::EXTENSION_JSON;
    }
} 