<?php

namespace Model\Source;


class Xml extends AbstractSource
{
    public function getData()
    {
        $xmlNodes = new \SimpleXMLElement(file_get_contents($this->getFilePath()));

        $allCurrencies = [];
        foreach ($xmlNodes as $node) {

            /** @var $node \SimpleXMLElement */
            $childrenNode = $node->children();
            $allCurrencies[] = [
                self::FIELD_GROUP => (string)$node->attributes()->Type,
                self::FIELD_CODE  => (string)$childrenNode->Code,
                self::FIELD_NAME  => (string)$childrenNode->Description,
                self::FIELD_PRICE => (float)$childrenNode->Value
            ];
        }

        return $allCurrencies;
    }

    protected function getFileExtension()
    {
        return self::EXTENSION_XML;
    }
} 