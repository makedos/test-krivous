<?php

namespace Model\Source;


class Php extends AbstractSource
{
    public function getData()
    {
        $data = include ($this->getFilePath());

        $allCurrencies = [];
        foreach ($data as $group => $currencies) {
            $currencyRow = [];
            $currencyRow[self::FIELD_GROUP] = $group;
            foreach ($currencies as $code => $currency) {
                $currencyRow[self::FIELD_CODE]  = $code;
                $currencyRow[self::FIELD_NAME]  = $currency['name'];
                $currencyRow[self::FIELD_PRICE] = $currency['value'];

                $allCurrencies[] = $currencyRow;
            }
        }

        return $allCurrencies;
    }

    protected function getFileExtension()
    {
        return self::EXTENSIION_PHP;
    }
} 