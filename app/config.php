<?php

return [
    'defaultController' => 'index',
    'defaultAction'     => 'index',

    'viewsPath' => __DIR__ . '/../views/',
    'appPath'   => __DIR__
];