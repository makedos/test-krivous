<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/7/14
 * Time: 10:53 PM
 */

namespace Library\Validator;


abstract class AbstractValidator implements ValidatorInterface
{
    /**
     * Message for not valid value
     * @var string
     */
    protected $errorMessage = '';

    /**
     * valid value
     * @var mixed
     */
    protected $value;

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @param string $errorMessage
     * @return $this
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
        return $this;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param $value
     * @return bool
     */
    abstract public function isValid($value);
} 