<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/8/14
 * Time: 7:02 AM
 */

namespace Library\Validator;

/**
 * If value is empty - validation is not need
 * Class EmptyOrValidator
 * @package Library\Validator
 */
class EmptyOrValidator extends AbstractValidator
{
    private $validator;

    function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function isValid($value)
    {
        if (empty($value)) {
            $isValid = true;
        } else {
            $isValid = $this->validator->isValid($value);

            $this
                ->setErrorMessage($this->validator->getErrorMessage())
                ->setValue($this->validator->getValue());
        }

        return $isValid;
    }
} 