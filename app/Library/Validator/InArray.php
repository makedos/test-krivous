<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/7/14
 * Time: 10:54 PM
 */

namespace Library\Validator;
/**
 * Checks that array contains value
 *
 * Class InArray
 * @package Library\Validator
 */
class InArray extends AbstractValidator
{
    protected $errorMessage = 'Value is not in valid array!';

    private $arrayOfValidValues;

    function __construct(array $arrayOfValidValues)
    {
        if (empty($arrayOfValidValues)) {
            throw new \InvalidArgumentException('Parameter $arrayOfValidValues can not be empty');
        }

        $this->arrayOfValidValues = $arrayOfValidValues;
    }

    public function isValid($value)
    {
        $this->setValue($value);
        return in_array($value, $this->arrayOfValidValues);
    }
} 