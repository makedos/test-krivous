<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/7/14
 * Time: 10:53 PM
 */

namespace Library\Validator;

/**
 * Checks value is float
 *
 * Class Float
 * @package Library\Validator
 */
class Float extends AbstractValidator
{
    const FLOAT_MIN = 0.000001;
    const FLOAT_MAX = 9999999.99;
    const FLOAT_MAX_LENGTH = 50;

    public function isValid($value)
    {
        $this->setValue(str_replace(',', '.', $value));
        if (!preg_match('/^(-)?([\d]+(\.)?|[\d]*\.)[\d]*$/', $this->getValue())) {
            $this->errorMessage = 'Value is not valid float';
            return false;
        }

        if (abs($this->getValue()) < self::FLOAT_MIN) {
            $this->errorMessage = 'Value is to small! Min is ' . self::FLOAT_MIN;
            return false;
        }

        if (abs($this->getValue()) > self::FLOAT_MAX) {
            $this->errorMessage = 'Value is to big! Max is ' . self::FLOAT_MAX;
            return false;
        }

        if (mb_strlen($this->getValue()) > self::FLOAT_MAX_LENGTH) {
            $this->errorMessage = 'Value is to long! Max length is ' . self::FLOAT_MAX_LENGTH;
            return false;
        }

        return true;
    }
} 