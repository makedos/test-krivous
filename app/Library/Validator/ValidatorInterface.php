<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/8/14
 * Time: 12:06 AM
 */

namespace Library\Validator;

/**
 * Interface for validation classes
 *
 * Interface ValidatorInterface
 * @package Library\Validator
 */
interface ValidatorInterface
{
    /**
     * Checks value is valid
     * @param $value
     * @return bool
     */
    public function isValid($value);

    /**
     * returns valid value
     * @return mixed
     */
    public function getValue();

    /**
     * Sets message for not valid value
     * @param string $errorMessage
     * @return $this
     */
    public function setErrorMessage($errorMessage);

    /**
     * Returns message for not valid value
     * @return string
     */
    public function getErrorMessage();
} 