<?php

namespace Library\Validator;

/**
 * Checks length of string
 *
 * Class StringLength
 * @package Library\Validator
 */
class StringLength extends AbstractValidator
{
    protected $errorMessage = 'Value has wrong length';

    /**
     * @var int
     */
    private $minLength;

    /**
     * @var int
     */
    private $maxLength;

    function __construct($minLength, $maxLength)
    {
        if ($minLength < 0) {
            throw new \InvalidArgumentException('Parameter $minLength can not be less than 0');
        }

        if ($maxLength == 0) {
            throw new \InvalidArgumentException('Parameter $maxLength can not 0');
        }

        if ($maxLength < $minLength) {
            throw new \InvalidArgumentException('Parameter $maxLength can not be less than $minLength');
        }

        $this->minLength = $minLength;
        $this->maxLength = $maxLength;
    }

    public function isValid($value)
    {
        $this->setValue($value);

        $strLength = mb_strlen($this->getValue());

        return $strLength >= $this->minLength && $strLength <= $this->maxLength;
    }
} 