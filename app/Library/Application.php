<?php

namespace Library;

use Library\Application\RouteNotFoundException;

class Application
{
    /**
     * @var string
     */
    private $controller;

    /**
     * @var string
     */
    private $action;

    /**
     * @var array
     */
    private $config;

    /**
     * @var \Twig_Environment
     */
    private $view;

    /**
     * @param $config array
     */
    function __construct($config)
    {
        $this->config = $config;

        $requestUriPath  = explode('/', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
        $this
            ->setController($requestUriPath)
            ->setAction($requestUriPath)
            ->setView();
    }

    /**
     * Getting controller and action from request and call them
     * @return string
     * @throws Application\RouteNotFoundException
     */
    public function run()
    {
        $controllerClass = '\\Controller\\' . ucfirst($this->controller);
        if (!class_exists($controllerClass)) {
            throw new RouteNotFoundException('Wrong controller name!');
        }

        $controller = new $controllerClass();

        $actionMethod = $this->action . 'Action';
        if (!method_exists($controller, $actionMethod)) {
            throw new RouteNotFoundException('Wrong action name!');
        }

        $viewData = $controller->{$actionMethod}();

        return $this->view->render(
            $this->controller . '/' . $this->action . '.phtml',
            is_array($viewData) ? $viewData : []
        );
    }

    private function setController($requestUriPath)
    {
        $controllerKey = 1;
        $this->controller = isset($requestUriPath[$controllerKey]) && !empty($requestUriPath[$controllerKey])
            ? strtolower($requestUriPath[$controllerKey])
            : $this->config['defaultAction'];

        return $this;
    }

    private function setAction($requestUriPath)
    {
        $actionKey = 2;
        $this->action = isset($requestUriPath[$actionKey]) && !empty($requestUriPath[$actionKey])
            ? strtolower($requestUriPath[$actionKey])
            : $this->config['defaultAction'];

        return $this;
    }

    private function setView()
    {
        $this->view = new \Twig_Environment(
            new \Twig_Loader_Filesystem($this->config['viewsPath'])
        );

        return $this;
    }
} 