<?php

namespace Library\Application\Controller;

/**
 * Class BaseController
 * @package Library\Application\Controller
 */
class BaseController
{
    protected function getPost()
    {
        return $_POST;
    }

    protected function getParams()
    {
        return $_REQUEST;
    }

    protected function isPost()
    {
        return 'POST' === $_SERVER['REQUEST_METHOD'];
    }
} 