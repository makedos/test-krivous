<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/7/14
 * Time: 2:13 PM
 */

namespace Library\Application;

/**
 * Class RouteNotFoundException
 * @package Library\Application
 */
class RouteNotFoundException extends \Exception
{
    protected $code = '404';
} 