<?php

namespace Library\ArrayFilter;

/**
 * Filters array from elements,
 * which has field $comparedField that less than $comparedValue
 *
 * Class Less
 * @package Library\ArrayFilter
 */
class Less extends AbstractArrayFilter
{
    public function filter(array $data)
    {
        return array_filter($data,
            function ($value) {

                if (!isset($value[$this->comparedField])) {
                    throw new \InvalidArgumentException(
                        'Parameter $comparedField must be a key of array of second level of $data!'
                    );
                }

                return $value[$this->comparedField] < $this->comparedValue;
            }
        );
    }
} 