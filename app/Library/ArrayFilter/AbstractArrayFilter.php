<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/7/14
 * Time: 11:01 PM
 */

namespace Library\ArrayFilter;

/**
 * Abstract class of filtration 2-dimensional array
 *
 * Class AbstractArrayFilter
 * @package Library\ArrayFilter
 */
abstract class AbstractArrayFilter
{
    const FILTER_TYPE_MORE = 'more';
    const FILTER_TYPE_LESS = 'less';
    const FILTER_TYPE_LIKE = 'like';
    const FILTER_TYPE_EQUALS = 'equals';

    /**
     * Key in array.
     * Will filter by value got by this key from array
     * @var string
     */
    protected $comparedField;

    /**
     * With this value will compare when filtering
     * @var mixed
     */
    protected $comparedValue;

    /**
     * @param $comparedField
     * @return $this
     */
    public function setComparedField($comparedField)
    {
        $this->comparedField = $comparedField;
        return $this;
    }

    /**
     * @param $comparedValue
     * @return $this
     */
    public function setComparedValue($comparedValue)
    {
        $this->comparedValue = $comparedValue;
        return $this;
    }

    /**
     * Factory method for ArrayFilter classes
     *
     * @param $filterType
     * @return Equals|Less|Like|More
     * @throws \InvalidArgumentException
     */
    public static function factory($filterType)
    {
        switch($filterType) {
            case self::FILTER_TYPE_LESS:
                return new Less();
            case self::FILTER_TYPE_MORE:
                return new More();
            case self::FILTER_TYPE_LIKE:
                return new Like();
            case self::FILTER_TYPE_EQUALS:
                return new Equals();
            default:
                throw new \InvalidArgumentException('Wrong filter type!');
        }
    }

    abstract public function filter(array $data);
} 