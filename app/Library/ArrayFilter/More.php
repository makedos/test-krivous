<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/7/14
 * Time: 11:01 PM
 */

namespace Library\ArrayFilter;

/**
 * Filters array from elements,
 * which has field $comparedField that more than $comparedValue
 *
 * Class More
 * @package Library\ArrayFilter
 */
class More extends AbstractArrayFilter
{
    public function filter(array $data)
    {
        return array_filter($data,
            function ($value) {

                if (!isset($value[$this->comparedField])) {
                    throw new \InvalidArgumentException(
                        'Parameter $comparedField must be a key of array of second level of $data!'
                    );
                }

                return $value[$this->comparedField] > $this->comparedValue;
            }
        );
    }
} 