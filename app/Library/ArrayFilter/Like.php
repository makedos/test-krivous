<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/7/14
 * Time: 11:02 PM
 */

namespace Library\ArrayFilter;

/**
 * Filters array from elements,
 * which has field $comparedField that contain $comparedValue
 *
 * Class Like
 * @package Library\ArrayFilter
 */
class Like extends AbstractArrayFilter
{
    public function filter(array $data)
    {
        return array_filter($data,
            function ($value) {

                if (!isset($value[$this->comparedField])) {
                    throw new \InvalidArgumentException(
                        'Parameter $comparedField must be a key of array of second level of $data!'
                    );
                }

                return mb_strpos($value[$this->comparedField], $this->comparedValue) !== false;
            }
        );
    }
} 