<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/8/14
 * Time: 4:24 AM
 */

namespace Library\ArraySort;

/**
 * Class for sorting 2-dimensional array with string values
 *
 * Class String
 * @package Library\ArraySort
 */
class String extends AbstractArraySort
{
    public function sort($data)
    {
        uasort($data, function ($a, $b) {

            if (!isset($a[$this->field]) || !isset($b[$this->field])) {
                throw new \InvalidArgumentException(
                    'Parameter $field must be a key of array of second level of $data!'
                );
            }

            $result = strnatcasecmp(
                $a[$this->field],
                $b[$this->field]
            );

            return $this->method == self::SORT_METHOD_ASC
                ? $result
                : -$result;
        });

        return $data;
    }
} 