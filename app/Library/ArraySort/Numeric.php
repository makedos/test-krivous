<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/8/14
 * Time: 4:25 AM
 */

namespace Library\ArraySort;

/**
 * Class for sorting 2-dimensional array with numeric values
 *
 * Class Numeric
 * @package Library\ArraySort
 */
class Numeric extends AbstractArraySort
{
    public function sort($data)
    {
        uasort($data, function ($a, $b) {

            if (!isset($a[$this->field]) || !isset($b[$this->field])) {
                throw new \InvalidArgumentException(
                    'Parameter $field must be a key of array of second level of $data!'
                );
            }

            switch(true) {
                case $a[$this->field] == $b[$this->field]:
                    $result = 0;
                    break;
                case $a[$this->field] < $b[$this->field]:
                    $result = -1;
                    break;
                case $a[$this->field] > $b[$this->field]:
                    $result = 1;
                    break;
            }

            return $this->method == self::SORT_METHOD_ASC
                ? $result
                : -$result;
        });

        return $data;
    }
} 