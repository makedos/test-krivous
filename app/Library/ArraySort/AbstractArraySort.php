<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/8/14
 * Time: 4:24 AM
 */

namespace Library\ArraySort;

/**
 * Abstract class for sorting 2-dimentional array
 *
 * Class AbstractArraySort
 * @package Library\ArraySort
 */
abstract class AbstractArraySort
{
    const SORT_METHOD_ASC  = 'asc';
    const SORT_METHOD_DESC = 'desc';

    const SORT_TYPE_NUMERIC = 'numeric';
    const SORT_TYPE_STRING  = 'string';

    /**
     * Method of sorting asc or desc
     * @var string
     */
    protected $method = self::SORT_METHOD_ASC;

    /**
     * Key in array.
     * Will sort by value of array got by this key
     * @var string
     */
    protected $field;

    /**
     * @param $method
     * @return $this
     */
    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @param $field
     * @return $this
     */
    public function setField($field)
    {
        $this->field = $field;
        return $this;
    }

    /**
     * Factory method for ArraySort classes
     *
     * @param $sortType
     * @return Numeric|String
     * @throws \InvalidArgumentException
     */
    public static function factory($sortType)
    {
        switch ($sortType) {
            case self::SORT_TYPE_NUMERIC:
                return new Numeric();
            case self::SORT_TYPE_STRING:
                return new String();
            default:
                throw new \InvalidArgumentException('Wrong sort type!');
        }
    }

    /**
     * Method implements logic of sorting an array
     * @param $data
     * @return mixed
     */
    abstract public function sort($data);
} 