<?php

namespace Controller;

use Library\Application\Controller\BaseController;
use Model\Dictionary;
use Service\Currencies\CurrenciesService;
use Service\Currencies\GetCurrenciesParams;
use Service\Currencies\GetCurrenciesResult;

class Index extends BaseController
{
    public function indexAction()
    {
        if ($this->isPost()) {

            $params = new GetCurrenciesParams();
            $params->setFromArray($this->getPost());
            $params->dictionary = new Dictionary();

            /** @var GetCurrenciesResult $result */
            $result = $params->validate();

            if (!$result->hasErrors()) {
                $service = new CurrenciesService();
                $result->currencies = $service->getCurrencies($params);
            }

            return [
                'params'     => $params,
                'currencies' => $result->currencies,
                'errors'     => $result->getErrorMessages()
            ];
        }
    }
} 