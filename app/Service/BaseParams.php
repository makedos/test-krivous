<?php

namespace Service;

use Library\Validator\ValidatorInterface;

/**
 * Base class of parameters
 *
 * Class BaseParams
 * @package Service
 */
class BaseParams
{
    /**
     * @var ValidatorInterface[]
     */
    protected $validators = [];

    /**
     * @var string
     */
    protected $resultClassName = '\Service\BaseResult';

    /**
     * Validates param values
     * @return BaseResult
     */
    public function validate()
    {
        /** @var \Service\BaseResult $result */
        $result = new $this->resultClassName();

        $validators = $this->getValidators();
        array_walk(
            $validators,
            function (ValidatorInterface $validator, $proprety) use (&$result) {

                if (!$validator->isValid($this->{$proprety})) {
                    $result->addErrorMessage($validator->getErrorMessage(), $proprety);
                } else {
                    $this->{$proprety} =$validator->getValue();
                }
            }
        );

        return $result;
    }

    /**
     * @return \Library\Validator\ValidatorInterface[]
     */
    protected function getValidators()
    {
        return $this->validators;
    }

    /**
     * @param ValidatorInterface $validator
     * @param $property
     * @return $this
     * @throws \InvalidArgumentException
     */
    protected function addValidator(ValidatorInterface $validator, $property)
    {
        if (property_exists($this, $property)) {
            $this->validators[$property] = $validator;
        } else {
            throw new \InvalidArgumentException("Undefined property " . $property . ' in '. get_class($this));
        }

        return $this;
    }

    /**
     * @param $data
     * @return $this
     */
    public function setFromArray($data)
    {
        foreach ($data as $key => $value) {
            if (property_exists($this, $key) && !is_null($value)) {
                $this->{$key} = $value;
            }
        }

        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return get_object_vars($this);
    }
}