<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/7/14
 * Time: 11:49 PM
 */

namespace Service\Currencies;

use Library\ArrayFilter\AbstractArrayFilter;
use Library\ArraySort\AbstractArraySort;
use Model\Source\AbstractSource;
use Service\BaseService;

/**
 * Service for working with currencies.
 * Works with valid data
 *
 * Class CurrenciesService
 * @package Service\Currencies
 */

class CurrenciesService
{
    /**
     * Get filtered and sorted array of currencies
     * @param GetCurrenciesParams $params
     * @return array
     */
    public function getCurrencies(GetCurrenciesParams $params)
    {
        $currencies = $this->getSource($params->dataSource)->getData();

        if ($params->dataGroup) {

            $groupArrayFilter = $this->getFilter(
                AbstractArrayFilter::FILTER_TYPE_EQUALS,
                AbstractSource::FIELD_GROUP,
                $params->dataGroup
            );

            $currencies = $groupArrayFilter->filter($currencies);
        }

        if ($params->filterField && $params->filterType && $params->filterValue) {

            $arrayFilter = $this->getFilter(
                $params->filterType,
                $params->filterField,
                $params->filterValue
            );

            $currencies = $arrayFilter->filter($currencies);
        }

        if ($params->sortField && $params->sortMethod) {
            $sorter = $this->getSorter($params->sortField, $params->sortMethod);
            $currencies = $sorter->sort($currencies);
        }

        return $currencies;
    }

    /**
     * @param $type
     * @param $field
     * @param $value
     * @return AbstractArrayFilter
     */
    protected function getFilter($type, $field, $value)
    {
        $filter = AbstractArrayFilter::factory($type);
        $filter
            ->setComparedField($field)
            ->setComparedValue($value);

        return $filter;
    }

    /**
     * @param $field
     * @param $method
     * @return AbstractArraySort
     */
    protected function getSorter($field, $method)
    {
        $sortMap = [
            AbstractSource::FIELD_NAME  => AbstractArraySort::SORT_TYPE_STRING,
            AbstractSource::FIELD_CODE  => AbstractArraySort::SORT_TYPE_STRING,
            AbstractSource::FIELD_PRICE => AbstractArraySort::SORT_TYPE_NUMERIC
        ];

        $sorter = AbstractArraySort::factory($sortMap[$field]);
        $sorter
            ->setField($field)
            ->setMethod($method);

        return $sorter;
    }

    /**
     * @param $dataSource
     * @return \Model\Source\AbstractSource
     */
    protected function getSource($dataSource)
    {
        return AbstractSource::factory($dataSource);
    }
} 