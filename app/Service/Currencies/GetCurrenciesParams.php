<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/7/14
 * Time: 11:48 PM
 */

namespace Service\Currencies;


use Library\ArrayFilter\AbstractArrayFilter;
use Library\Validator\EmptyOrValidator;
use Library\Validator\Float;
use Library\Validator\InArray;
use Library\Validator\StringLength;
use Model\Dictionary;
use Model\Source\AbstractSource;
use Service\BaseParams;

class GetCurrenciesParams extends BaseParams
{
    const MAX_FILTER_VALUE_LENGTH = 50;
    const MIN_FILTER_VALUE_LENGTH = 1;

    /**
     * @var string
     */
    public $dataSource;

    /**
     * @var string
     */
    public $dataGroup;

    /**
     * @var string
     */
    public $filterField;

    /**
     * @var string
     */
    public $filterType;

    /**
     * @var float|string
     */
    public $filterValue;

    /**
     * @var string
     */
    public $sortField;

    /**
     * @var string
     */
    public $sortMethod;

    /**
     * @var Dictionary
     */
    public $dictionary;

    protected $resultClassName = '\Service\Currencies\GetCurrenciesResult';

    protected function getValidators()
    {
        return $this
            ->addValidator($this->getDataSourceValidator(),  'dataSource')
            ->addValidator($this->getDataGroupVAlidator(),   'dataGroup')
            ->addValidator($this->getFilterFieldValidator(), 'filterField')
            ->addValidator($this->getFilterTypeValidator(),  'filterType')
            ->addValidator($this->getSortFieldValidator(),   'sortField')
            ->addValidator($this->getSortMethodValidator(),  'sortMethod')
            ->addValidator($this->getFilterValueValidator(), 'filterValue')
            ->validators;
    }

    private function getFilterValueValidator()
    {
        if ($this->filterField == AbstractSource::FIELD_PRICE) {
            $validator = new Float();
        } else {
            $validator = new StringLength(self::MIN_FILTER_VALUE_LENGTH, self::MAX_FILTER_VALUE_LENGTH);
        }

        return new EmptyOrValidator($validator);
    }

    private function getDataSourceValidator()
    {
        return new InArray($this->dictionary->getSources());
    }

    private function getDataGroupVAlidator()
    {
        return new EmptyOrValidator(new InArray($this->dictionary->getGroups()));
    }

    private function getFilterFieldValidator()
    {
       return new EmptyOrValidator(new InArray($this->dictionary->getFilterFields()));
    }

    private function getFilterTypeValidator()
    {
        return new EmptyOrValidator(new InArray($this->dictionary->getFilterTypes()));
    }

    private function getSortFieldValidator()
    {
        return new EmptyOrValidator(new InArray($this->dictionary->getFilterFields()));
    }

    private function getSortMethodValidator()
    {
        return new InArray($this->dictionary->getSortMethods());
    }
} 