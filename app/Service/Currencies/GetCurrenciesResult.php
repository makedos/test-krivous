<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/8/14
 * Time: 4:09 AM
 */

namespace Service\Currencies;


use Service\BaseResult;

class GetCurrenciesResult extends BaseResult
{
    /**
     * @var array
     */
    public $currencies = [];
} 