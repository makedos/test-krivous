<?php

namespace Service;

/**
 * Base class of Result
 *
 * Class BaseResult
 * @package Service
 */
class BaseResult
{
    /**
     * @var array
     */
    private $errorMessages = [];

    /**
     * @param string $errorMessage
     * @param null $key
     * @return $this
     */
    public function addErrorMessage($errorMessage, $key = null)
    {
        if (is_null($key)) {
            $this->errorMessages[] = $errorMessage;
        } else {
            $this->errorMessages[$key] = $errorMessage;
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function hasErrors()
    {
        return !empty($this->errorMessages);
    }
} 