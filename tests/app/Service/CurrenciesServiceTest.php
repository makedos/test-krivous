<?php

namespace Service;

use Model\Source\AbstractSource;
use Service\Currencies\GetCurrenciesParams;

class CurrenciesServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider providerTestGetCurrencies
     * @covers \Service\Currencies\CurrenciesService
     * @param $inputData
     * @param $expectedData
     */
    public function testGetCurrencies($inputData, $expectedData)
    {
        $params = new GetCurrenciesParams();
        $params->setFromArray($inputData);
        $service = $this->getService(
            ['data' => ['data']], ['filteredData' => ['data']], ['sortedData' => ['data']]
        );

        $this->assertEquals($service->getCurrencies($params), $expectedData['data']);
    }

    public function providerTestGetCurrencies()
    {
        return [
            1 => [
                [
                    'dataSource'  => AbstractSource::EXTENSIION_PHP
                ],
                [
                    'data' => ['data' => ['data']]
                ]
            ],
            2 => [
                [
                    'dataSource'  => AbstractSource::EXTENSIION_PHP,
                    'dataGroup'   => 'world',
                ],
                [
                    'data' => ['filteredData' => ['data']]
                ]
            ],
            3 => [
                [
                    'dataSource'  => AbstractSource::EXTENSIION_PHP,
                    'filterField' => 'filter',
                    'filterType'  => 'type',
                    'filterValue' => 'value',
                ],
                [
                    'data' => ['filteredData' => ['data']],
                ]
            ],
            4 => [
                [
                    'dataSource'  => AbstractSource::EXTENSIION_PHP,
                    'sortField'   => 'sortField',
                    'sortMethod'  => 'asc'
                ],
                [
                    'data' => ['sortedData' => ['data']],
                ]
            ]
        ];
    }

    private function getService($data, $filterData, $sortData)
    {
        $service = $this->getMock(
            '\Service\Currencies\CurrenciesService',
            ['getFilter', 'getSorter', 'getSource']
        );

        $service
            ->expects($this->any())
            ->method('getSource')
            ->will($this->returnValue($this->getSourceMock($data)));

        $service
            ->expects($this->any())
            ->method('getFilter')
            ->will($this->returnValue($this->getFilterMock($filterData)));

        $service
            ->expects($this->any())
            ->method('getSorter')
            ->will($this->returnValue($this->getSorterMock($sortData)));

        return $service;
    }

    private function getFilterMock($returnValue)
    {
        $filterMock = $this->getMockForAbstractClass(
            '\Library\ArrayFilter\AbstractArrayFilter',
            [], '',  true, true, true,
            ['filter']
        );

        $filterMock
            ->expects($this->any())
            ->method('filter')
            ->will($this->returnValue($returnValue));

        return $filterMock;
    }

    private function getSorterMock($returnValue)
    {
        $sorterMock = $this->getMockForAbstractClass(
            '\Library\ArraySort\AbstractArraySort',
            [], '',  true, true, true,
            ['sort']
        );

        $sorterMock
            ->expects($this->any())
            ->method('sort')
            ->will($this->returnValue($returnValue));

        return $sorterMock;
    }

    private function getSourceMock($returnValue)
    {
        $sourceMock = $this->getMockForAbstractClass(
            '\Model\Source\AbstractSource',
            [], '',  true, true, true,
            ['getData']
        );

        $sourceMock
            ->expects($this->any())
            ->method('getData')
            ->will($this->returnValue($returnValue));

        return $sourceMock;
    }
} 