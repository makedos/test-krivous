<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 9/8/14
 * Time: 10:21 PM
 */

namespace Service;


use Library\ArrayFilter\AbstractArrayFilter;
use Model\Dictionary;
use Model\Source\AbstractSource;
use Service\Currencies\GetCurrenciesParams;

class GetCurrenciesParamsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider dataProviderGetCurrenciesParams
     * @covers \Service\Currencies\GetCurrenciesParams
     * @param $inputData
     * @param $expectedPropertiesWithErrors
     */
    public function testValidate($inputData, $expectedPropertiesWithErrors)
    {
        $params = new GetCurrenciesParams();
        $params->setFromArray($inputData);
        $params->dictionary = new Dictionary();

        $result = $params->validate();
        $actualPropertiesWithErrors = array_keys($result->getErrorMessages());

        sort($actualPropertiesWithErrors);
        sort($expectedPropertiesWithErrors);
        $this->assertEquals(
            $expectedPropertiesWithErrors,
            $actualPropertiesWithErrors
        );
    }

    public function dataProviderGetCurrenciesParams()
    {
        return [
            1 => [
                [
                   'dataSource'  => AbstractSource::EXTENSIION_PHP,
                   'dataGroup'   => '',
                   'filterField' => '',
                   'filterType'  => '',
                   'filterValue' => '',
                   'sortField'   => '',
                   'sortMethod'  => 'asc'
                ],
                []
            ],
            2 => [
                [
                    'dataSource'  => AbstractSource::EXTENSIION_PHP,
                    'dataGroup'   => 'world',
                    'filterField' => '',
                    'filterType'  => '',
                    'filterValue' => '',
                    'sortField'   => '',
                    'sortMethod'  => 'asc'
                ],
                []
            ],
            3 => [
                [
                    'dataSource'  => AbstractSource::EXTENSIION_PHP,
                    'dataGroup'   => 'world',
                    'filterField' => AbstractSource::FIELD_PRICE,
                    'filterType'  => AbstractArrayFilter::FILTER_TYPE_MORE,
                    'filterValue' => 2.5,
                    'sortField'   => AbstractSource::FIELD_PRICE,
                    'sortMethod'  => 'desc'
                ],
                []
            ],
            4 => [
                [
                    'dataSource'  => AbstractSource::EXTENSIION_PHP,
                    'dataGroup'   => 'world',
                    'filterField' => AbstractSource::FIELD_NAME,
                    'filterType'  => AbstractArrayFilter::FILTER_TYPE_LIKE,
                    'filterValue' => 'blabla',
                    'sortField'   => AbstractSource::FIELD_PRICE,
                    'sortMethod'  => 'desc'
                ],
                []
            ],
            5 => [
                [
                    'dataSource'  => '',
                    'dataGroup'   => 'world',
                    'filterField' => AbstractSource::FIELD_NAME,
                    'filterType'  => AbstractArrayFilter::FILTER_TYPE_LIKE,
                    'filterValue' => 'blabla',
                    'sortField'   => AbstractSource::FIELD_PRICE,
                    'sortMethod'  => 'desc'
                ],
                [
                    'dataSource'
                ]
            ],
            6 => [
                [
                    'dataSource'  => AbstractSource::EXTENSIION_PHP,
                    'dataGroup'   => 'ppp',
                    'filterField' => 'fdfdf',
                    'filterType'  => 'sfee',
                    'filterValue' => 'blablavvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv',
                    'sortField'   => 'cdcdcdcdcdcdcdcdc',
                    'sortMethod'  => 'dfsfsdfsdfsdfsd'
                ],
                [
                    'dataGroup',
                    'filterField',
                    'filterType',
                    'filterValue',
                    'sortField',
                    'sortMethod'
                ]
            ]
        ];
    }
} 