<?php

namespace Library;

use Library\ArrayFilter\AbstractArrayFilter;
use Library\ArrayFilter\Equals;
use Library\ArrayFilter\Less;
use Library\ArrayFilter\Like;
use Library\ArrayFilter\More;

class ArrayFilterTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test of any filter
     * @param $filter
     * @param $value
     * @param $field
     * @param $filterData
     * @param $expectedData
     */
    private function filterTest($filter, $value, $field, $filterData, $expectedData)
    {
        /** @var $filter AbstractArrayFilter */
        $filter
            ->setComparedValue($value)
            ->setComparedField($field);

        $this->assertEquals($expectedData, $filter->filter($filterData));
    }

    /**
     * @dataProvider dataProviderFilterEquals
     * @covers \Library\ArrayFilter\Equals
     * @param $inputData
     * @param $expectedData
     */
    public function testFilterEquals($inputData, $expectedData)
    {
        $this->filterTest(
            new Equals(), $inputData['value'], $inputData['field'], $inputData['filterData'], $expectedData
        );
    }

    public function dataProviderFilterEquals()
    {
        $filterData = [
            ['filterField' => '1test', 'filterField1'=> 1],
            ['filterField' => 'test',  'filterField1'=> 2],
            ['filterField' => 'test1', 'filterField1'=> 3],
            ['filterField' => 'test',  'filterField1'=> 4],
            ['filterField' => 'blaa',  'filterField1'=> 5],
        ];

        return [
            1 => [
                [
                    'value' => 'test',
                    'field' => 'filterField',
                    'filterData' => $filterData
                ],
                [
                    1 => ['filterField' => 'test',  'filterField1'=> 2],
                    3 => ['filterField' => 'test',  'filterField1'=> 4],
                ]
            ],

            2 => [
                [
                    'value' => 5,
                    'field' => 'filterField1',
                    'filterData' => $filterData
                ],
                [
                    4 => ['filterField' => 'blaa',  'filterField1'=> 5],
                ]
            ],
            3 => [
                [
                    'value' => 11,
                    'field' => 'filterField1',
                    'filterData' => $filterData
                ],
                []
            ]
        ];
    }

    /**
     * @dataProvider dataProviderFilterLess
     * @covers \Library\ArrayFilter\Less
     * @param $inputData
     * @param $expectedData
     */
    public function testFilterLess($inputData, $expectedData)
    {
        $this->filterTest(
            new Less(), $inputData['value'], $inputData['field'], $inputData['filterData'], $expectedData
        );
    }

    public function dataProviderFilterLess()
    {
        $filterData = [
            ['filterField' => '1.5',  'filterField1'=> 1],
            ['filterField' => '-2.9', 'filterField1'=> 2],
            ['filterField' => '34.5', 'filterField1'=> 3],
            ['filterField' =>  81.70, 'filterField1'=> 4],
            ['filterField' =>  -5,    'filterField1'=> 5],
        ];

        return [
            1 => [
                [
                    'value' => 30,
                    'field' => 'filterField',
                    'filterData' => $filterData
                ],
                [
                    0 => ['filterField' => '1.5',  'filterField1'=> 1],
                    1 => ['filterField' => '-2.9', 'filterField1'=> 2],
                    4 => ['filterField' =>  -5,    'filterField1'=> 5],
                ]
            ],

            2 => [
                [
                    'value' => 3,
                    'field' => 'filterField1',
                    'filterData' => $filterData
                ],
                [
                    0 => ['filterField' => '1.5',  'filterField1'=> 1],
                    1 => ['filterField' => '-2.9', 'filterField1'=> 2],
                ]
            ],
            3 => [
                [
                    'value' => -100,
                    'field' => 'filterField1',
                    'filterData' => $filterData
                ],
                []
            ]
        ];
    }

    /**
     * @dataProvider dataProviderFilterLike
     * @covers \Library\ArrayFilter\Like
     * @param $inputData
     * @param $expectedData
     */
    public function testFilterLike($inputData, $expectedData)
    {
        $this->filterTest(
            new Like(), $inputData['value'], $inputData['field'], $inputData['filterData'], $expectedData
        );
    }

    public function dataProviderFilterLike()
    {
        $filterData = [
            ['filterField' => 'krawat',     'filterField1'=> 1],
            ['filterField' => 'dziadkowie', 'filterField1'=> 2],
            ['filterField' => 'kobieta',    'filterField1'=> 3],
            ['filterField' => 'czekolada',  'filterField1'=> 4],
            ['filterField' => 'czeszć!',    'filterField1'=> 5],
        ];

        return [
            1 => [
                [
                    'value' => 'cze',
                    'field' => 'filterField',
                    'filterData' => $filterData
                ],
                [
                    3 => ['filterField' => 'czekolada',  'filterField1'=> 4],
                    4 => ['filterField' => 'czeszć!',    'filterField1'=> 5],
                ]
            ],

            2 => [
                [
                    'value' => 'bie',
                    'field' => 'filterField',
                    'filterData' => $filterData
                ],
                [
                    2 => ['filterField' => 'kobieta',    'filterField1'=> 3]
                ]
            ],
            3 => [
                [
                    'value' => 'krawat',
                    'field' => 'filterField',
                    'filterData' => $filterData
                ],
                [
                    0 => ['filterField' => 'krawat', 'filterField1'=> 1]
                ]
            ],

            4 => [
                [
                    'value' => 'nic',
                    'field' => 'filterField1',
                    'filterData' => $filterData
                ],
                [
                ]
            ]
        ];
    }

    /**
     * @dataProvider dataProviderFilterMore
     * @covers \Library\ArrayFilter\More
     * @param $inputData
     * @param $expectedData
     */
    public function testFilterMore($inputData, $expectedData)
    {
        $this->filterTest(
            new More(), $inputData['value'], $inputData['field'], $inputData['filterData'], $expectedData
        );
    }

    public function dataProviderFilterMore()
    {
        $filterData = [
            ['filterField' => '1.5',  'filterField1'=> 1],
            ['filterField' => '-2.9', 'filterField1'=> 2],
            ['filterField' => '34.5', 'filterField1'=> 3],
            ['filterField' =>  81.70, 'filterField1'=> 4],
            ['filterField' =>  -5,    'filterField1'=> 5],
        ];

        return [
            1 => [
                [
                    'value' => 30,
                    'field' => 'filterField',
                    'filterData' => $filterData
                ],
                [

                    2 => ['filterField' => '34.5', 'filterField1'=> 3],
                    3 => ['filterField' =>  81.70, 'filterField1'=> 4],
                ]
            ],

            2 => [
                [
                    'value' => 3,
                    'field' => 'filterField',
                    'filterData' => $filterData
                ],
                [
                    2 => ['filterField' => '34.5', 'filterField1'=> 3],
                    3 => ['filterField' =>  81.70, 'filterField1'=> 4],
                ]
            ],
            3 => [
                [
                    'value' => 100,
                    'field' => 'filterField1',
                    'filterData' => $filterData
                ],
                []
            ]
        ];
    }

    /**
     * @covers \Library\ArrayFilter\AbstractArrayFilter
     */
    public function testAbstractArrayFilterFactory()
    {
        $filterInstances = [
            AbstractArrayFilter::FILTER_TYPE_EQUALS => 'Library\ArrayFilter\Equals',
            AbstractArrayFilter::FILTER_TYPE_LIKE   => 'Library\ArrayFilter\Like',
            AbstractArrayFilter::FILTER_TYPE_MORE   => 'Library\ArrayFilter\More',
            AbstractArrayFilter::FILTER_TYPE_LESS   => 'Library\ArrayFilter\Less'
        ];

        foreach ($filterInstances as $filterType => $instance) {
            $this->assertInstanceOf(
                $instance,
                AbstractArrayFilter::factory($filterType)
            );
        }
    }

    /**
     * @expectedException \InvalidArgumentException
     * @covers \Library\ArrayFilter\AbstractArrayFilter
     */
    public function testAbstractArrayFilterFactoryException()
    {
        AbstractArrayFilter::factory('wrongFilterType');
    }
} 