<?php

namespace Library;

use Library\ArraySort\AbstractArraySort;
use Library\ArraySort\Numeric;
use Library\ArraySort\String;

class ArraySortTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @covers \Library\ArraySort\Numeric
     * @expectedException \InvalidArgumentException
     */
    public function testArraySortNumericException()
    {
        $sorter = new Numeric();
        $sorter->setField('test');
        $sorter->sort([['a' => 3], ['a' => 2]]);
    }

    /**
     * @dataProvider dataProviderArraySortNumeric
     * @covers \Library\ArraySort\Numeric
     * @param $inputData
     * @param $expectedData
     */
    public function testArraySortNumeric($inputData, $expectedData)
    {
        $sorter = new Numeric();
        $sorter
            ->setField($inputData['field'])
            ->setMethod($inputData['method']);

        $this->assertEquals($expectedData, $sorter->sort($inputData['sortData']));
    }

    public function dataProviderArraySortNumeric()
    {
        $sortData = [
            ['field1' => '12',  'field2' => 1],
            ['field1' => '143', 'field2' => 2],
            ['field1' => '1.2', 'field2' => 3],
            ['field1' => '-34', 'field2' => 4],
            ['field1' => '41',  'field2' => 5]
        ];
        $field = 'field1';

        return [
            1 => [
                [
                    'field'    => $field,
                    'method'   => AbstractArraySort::SORT_METHOD_ASC,
                    'sortData' => $sortData,
                ],
                [
                    3 => ['field1' => '-34', 'field2' => 4],
                    2 => ['field1' => '1.2', 'field2' => 3],
                    0 => ['field1' => '12',  'field2' => 1],
                    4 => ['field1' => '41',  'field2' => 5],
                    1 => ['field1' => '143', 'field2' => 2],
                ]
            ],
            2 => [
                [
                    'field'    => $field,
                    'method'   => AbstractArraySort::SORT_METHOD_DESC,
                    'sortData' => $sortData,
                ],
                [
                    1 => ['field1' => '143', 'field2' => 2],
                    4 => ['field1' => '41',  'field2' => 5],
                    0 => ['field1' => '12',  'field2' => 1],
                    2 => ['field1' => '1.2', 'field2' => 3],
                    3 => ['field1' => '-34', 'field2' => 4],
                ]
            ],
        ];
    }


    /**
     * @covers \Library\ArraySort\Numeric
     * @expectedException \InvalidArgumentException
     */
    public function testArraySortStringException()
    {
        $sorter = new String();
        $sorter->setField('test');
        $sorter->sort([['a' => 'a'], ['a' => 'b']]);
    }

    /**
     * @dataProvider dataProviderArraySortString
     * @covers \Library\ArraySort\String
     * @param $inputData
     * @param $expectedData
     */
    public function testArraySortString($inputData, $expectedData)
    {
        $sorter = new String();
        $sorter
            ->setField($inputData['field'])
            ->setMethod($inputData['method']);

        $this->assertEquals($expectedData, $sorter->sort($inputData['sortData']));
    }

    /**
     * @return array
     */
    public function dataProviderArraySortString()
    {
        $sortData = [
            ['field1' => 'Abcd', 'field2' => 1],
            ['field1' => 'bcd',  'field2' => 2],
            ['field1' => 'abcd', 'field2' => 3],
            ['field1' => 'aBcD', 'field2' => 4]
        ];
        $field = 'field1';

        return [
            1 => [
                [
                    'field'    => $field,
                    'method'   => \Library\ArraySort\AbstractArraySort::SORT_METHOD_ASC,
                    'sortData' => $sortData
                ],
                [
                    0 => ['field1' => 'Abcd', 'field2' => 1],
                    3 => ['field1' => 'aBcD', 'field2' => 4],
                    2 => ['field1' => 'abcd', 'field2' => 3],
                    1 => ['field1' => 'bcd',  'field2' => 2]
                ]
            ],
            2 => [
                [
                    'field'    => $field,
                    'method'   => AbstractArraySort::SORT_METHOD_DESC,
                    'sortData' => $sortData
                ],
                [

                    1 => ['field1' => 'bcd',  'field2' => 2],
                    2 => ['field1' => 'abcd', 'field2' => 3],
                    3 => ['field1' => 'aBcD', 'field2' => 4],
                    0 => ['field1' => 'Abcd', 'field2' => 1],
                ]
            ],
        ];
    }

    /**
     * @covers \Library\ArraySort\AbstractArraySort
     */
    public function testAbstractArraySortFactory()
    {
        $this->assertInstanceOf(
            'Library\ArraySort\Numeric',
            AbstractArraySort::factory(AbstractArraySort::SORT_TYPE_NUMERIC)
        );

        $this->assertInstanceOf(
            'Library\ArraySort\String',
            AbstractArraySort::factory(AbstractArraySort::SORT_TYPE_STRING)
        );
    }

    /**
     * @expectedException \InvalidArgumentException
     * @covers \Library\ArraySort\AbstractArraySort
     */
    public function testAbstractArraySortFactoryException()
    {
        AbstractArraySort::factory('wrongSortType');
    }
} 