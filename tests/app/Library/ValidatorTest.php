<?php
namespace Library;

use Library\Validator\EmptyOrValidator;
use Library\Validator\Float;
use Library\Validator\InArray;
use Library\Validator\StringLength;
use Library\Validator\ValidatorInterface;

class ValidatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @covers \Library\Validator\Float
     * @dataProvider dataProviderValidatorFloat
     * @param $inputData
     * @param $expectedData
     */
    public function testValidatorFloat($inputData, $expectedData)
    {
        $validator = new Float();
        $this->assertValueIsValid($validator, $inputData['value'], $expectedData['isValid']);

        $this->assertEquals(
            $expectedData['value'],
            $validator->getValue()
        );
    }

    /**
     * @return array
     */
    public function dataProviderValidatorFloat()
    {
        return [
            1 => [
                ['value' => '1',],
                ['isValid' => true, 'value' => '1']
            ],
            2 => [
                ['value' => '1,234'],
                ['isValid' => true, 'value' => '1.234']
            ],
            3 => [
                ['value' => '5999.9123'],
                ['isValid' => true, 'value' => '5999.9123']
            ],
            4 => [
                ['value' => 5999.9123],
                ['isValid' => true, 'value' => 5999.9123]
            ],
            5 => [
                ['value' => 5],
                ['isValid' => true, 'value' => 5]
            ],
            6 => [
                ['value' => 'asd'],
                ['isValid' => false, 'value' => 'asd']
            ],
            7 => [
                ['value' => '0.000000000001'],
                ['isValid' => false, 'value' => '0.000000000001']
            ],
            8 => [
                ['value' => '9999999.999'],
                ['isValid' => false, 'value' => '9999999.999']
            ],
        ];
    }

    /**
     * @dataProvider dataProviderValidatorInArray
     * @covers \Library\Validator\InArray
     * @param $inputData
     * @param $expectedData
     */
    public function testValidatorInArray($inputData, $expectedData)
    {
        $this->assertValueIsValid(
            new InArray($inputData['validArray']),
            $inputData['value'],
            $expectedData['isValid']
        );
    }


    /**
     * @expectedException \InvalidArgumentException
     * @covers \Library\Validator\InArray
     */
    public function testValidatorInArrayException()
    {
        new InArray([]);
    }

    public function dataProviderValidatorInArray()
    {
        return [
            1 => [
                ['validArray' => [1, 2, 4], 'value' => '4'],
                ['isValid' => true]
            ],
            2 => [
                ['validArray' => [1.2, 2.3, 4.45], 'value' => '4.45'],
                ['isValid' => true]
            ],
            3 => [
                ['validArray' => ['1.2', '3.5', '2.3'], 'value' => 3.5],
                ['isValid' => true]
            ],
            4 => [
                ['validArray' => [1, 2, 4], 'value' => 2],
                ['isValid' => true]
            ],
            5 => [
                ['validArray' => ['one', 'two', 'three'], 'value' => 'two'],
                ['isValid' => true]
            ],
            6 => [
                ['validArray' => [true, false], 'value' => false],
                ['isValid' => true]
            ],
            7 => [
                ['validArray' => ['a', 'b', 'c'], 'value' => 'd'],
                ['isValid' => false]
            ],
        ];
    }

    /**
     * @expectedException \InvalidArgumentException
     * @covers \Library\Validator\StringLength
     */
    public function testValidatorStringLengthExceptionMaxLength()
    {
        new StringLength(0, 0);
    }

    /**
     * @expectedException \InvalidArgumentException
     * @covers \Library\Validator\StringLength
     */
    public function testValidatorStringLengthExceptionMaxLessThanMin()
    {
        new StringLength(200, 100);
    }

    /**
     * @expectedException \InvalidArgumentException
     * @covers \Library\Validator\StringLength
     */
    public function testValidatorStringLengthTestExceptionMinLength()
    {
        new StringLength(-1, 1);
    }

    /**
     * @dataProvider dataProviderValidatorStringLength
     * @covers \Library\Validator\StringLength
     * @param $inputData
     * @param $expectedData
     */
    public function testValidatorStringLengthTest($inputData, $expectedData)
    {
        $this->assertValueIsValid(
            new StringLength($inputData['min'], $inputData['max']),
            $inputData['value'],
            $expectedData['isValid']
        );
    }

    public function dataProviderValidatorStringLength()
    {
        return [
            1 => [
                ['min' => 3, 'max' => 5, 'value' => 'asdf'],
                ['isValid' => true]
            ],
            2 => [
                ['min' => 3, 'max' => 5, 'value' => 'asd'],
                ['isValid' => true]
            ],
            3 => [
                ['min' => 3, 'max' => 5, 'value' => 'asdfa'],
                ['isValid' => true]
            ],
            4 => [
                ['min' => 5, 'max' => 10, 'value' => 'asdf'],
                ['isValid' => false]
            ],
            5 => [
                ['min' => 5, 'max' => 10, 'value' => 'asdfasasdcs'],
                ['isValid' => false]
            ],
        ];
    }

    /**
     * @dataProvider dataProviderEmptyOrValidator
     * @covers \Library\Validator\EmptyOrValidator
     * @param $inputData
     * @param $expectedData
     */
    public function testEmptyOrValidator($inputData, $expectedData)
    {
        $validatorMock = $this->getAbstractValidatorMock($inputData['value']);
        $validatorMock->setErrorMessage('Error!');

        $emptyOrValidator = new EmptyOrValidator($validatorMock);
        $emptyOrValidator->isValid($inputData['value']);

        $this->assertEquals($emptyOrValidator->getValue(), $expectedData['value']);
        $this->assertEquals($emptyOrValidator->getErrorMessage(), $expectedData['errorMessage']);
    }

    /**
     * @return array
     */
    public function dataProviderEmptyOrValidator()
    {
        return [
            1 => [
                ['value' => null],
                ['value' => null, 'errorMessage' => '']
            ],
            2 => [
                ['value' => false],
                ['value' => null, 'errorMessage' => '']
            ],
            3 => [
                ['value' => []],
                ['value' => null, 'errorMessage' => '']
            ],
            4 => [
                ['value' => 'a'],
                ['value' => 'a', 'errorMessage' => 'Error!']
            ],
        ];
    }

    /**
     * Return Mock of abstract validator
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    private function getAbstractValidatorMock($value = null)
    {
        $validatorMock = $this->getMockForAbstractClass(
            '\Library\Validator\AbstractValidator',
            [], '',  true, true, true,
            ['isValid', 'getValue']
        );

        $validatorMock
            ->expects($this->any())
            ->method('isValid')
            ->will($this->returnValue(true));
        $validatorMock
            ->expects($this->any())
            ->method('getValue')
            ->will($this->returnValue($value));


        return $validatorMock;
    }

    /**
     * Assert value is valid or is not valid
     * @param ValidatorInterface $validator
     * @param $isValid
     * @param $value
     */
    private function assertValueIsValid($validator, $value, $isValid)
    {
        $this->assertEquals(
            $isValid,
            $validator->isValid($value)
        );
    }
} 